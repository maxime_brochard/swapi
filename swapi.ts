function requete(resource) {

    fetch(`https://swapi.co/api/${resource}/`).then(function (response) {
        return response.json();
    }).then(function (json) {

            let data = parseData(json, resource);
            displayResult(data);
        }
    )
}


function parseData(json, resourceType) {
    let array = [];
    console.log(json.results.length);
    for (let i = 0; i < json.results.length; i++) {

        //let line = document.createElement("DIV");
        let item = json.results[i];

        switch (resourceType) {
            case 'people':
                array[i] =
                    {
                        "name": item.name,
                        "height (cm)": item.height,
                        "DOB": item.birth_year,
                        "mass (kg)": item.mass
                    }
                break;

            case 'planets':
                array[i] =
                    {
                        "name": item.name,
                        "rot. period (h)": item.rotation_period,
                        "orb. period (D)": item.orbital_period,
                        "diam. (Kms)": item.diameter
                    }

                break;

            case 'starships':
                array[i] =
                    {
                        "name": item.name,
                        "class": item.starship_class,
                        "manuf.": item.manufacturer,
                        "length (m)": item.length
                    }

                break;

        }
    }

    console.log(array);
    return array;
}


function displayResult(array) {

    document.getElementById('resultUl').innerHTML = "";

    for (var i = 0, l = array.length; i < l; i++) {
        const arrayItem = array[i];


        let name = arrayItem['name'];
        let spanList = '';
        let x = 0;
        Object.keys(arrayItem).forEach(function (item) {
            let badgeClass = definePillColor(x);
            spanList = spanList + `<span class="badge badge-pill badge-${badgeClass} badge-${x}">${item} : ${arrayItem[item]}</span>`;
            x++;
        });


        let line = document.createElement("DIV");

        line.innerHTML = `
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div class="itemName">${name}</div>
                        <div class="pillDiv"> 
                           ${spanList}
                        </div>
                    </li>`;

        //print line
        document.getElementById('resultUl').appendChild(line);


    }

}

function definePillColor(x: number) {
    switch (x) {
        case 1:
            return 'warning';
            break;
        case 2:
            return 'info';
            break;
        case 3:
            return 'secondary';
            break;
        case 4:
            return 'success';
            break;

        default:
            return 'primary';

    }
}